#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Módulo que inclyde la Clase y el programa main para un servidor SIP."""

import sys
import os
import socketserver


class ServerHandler(socketserver.DatagramRequestHandler):
    """Clase que simula el servidor SIP."""

    def handle(self):
        """Método que se usa para hacer que el servidor reciba y responda a una petición"""
        # mandamos una señal indicando la recepción de la peticion
        self.wfile.write(b"Servidor: Hemos recibido la peticion. Respuesta: \n ")
        # obtenemos la peticion del cliente
        decoded_line = self.rfile.read()
        data = decoded_line.decode('utf-8')
        print("El cliente nos manda: \n", data)
        # comrpobamos el método usado por el cliente
        METODO = data.split(' ')[0]
        # y pasamos a enviar las corresponidentes señales en función del método
        if METODO in ('ACK', 'BYE',  'INVITE'):
            if METODO == 'INVITE':
                self.invite_handler()
            if METODO == 'ACK':
                self.ack_handler()
            else:
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        elif METODO not in ('ACK', 'BYE',  'INVITE'):
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")

# /////////////////////////////////////////////////////////////////////////////////////////////////
    # A continuación encontramos una serie de metodos auxiliares que nos ayudaran a embellecer el código y
    # mejorar su lectura, evitrando tener funciones largas que no cumplan los estandares PEP-8
    # ---------------------------------------------------------------------------------------------
    def invite_handler(self):
        """Método que se usa para hacer que el servidor reciba y responda a una petición INVITE"""
        self.wfile.write(b"SIP/2.0 100 Trying\r\n\r\n")
        self.wfile.write(b"SIP/2.0 180 Ringing\r\n\r\n")

    # ----------------------------------------------------------------------------------------------
    def ack_handler(self):
        """Método que se usa para hacer que el servidor reciba y responda a una petición ACK"""
        ejecucion = 'mp32rtp -i ' + IP + ' -p 23032 < ' + AUDIO
        print("Porocedemos a ejecutar el siguiente archivo de audio: ", ejecucion)
        os.system(ejecucion)


# ///////////////////////////////////////////////////////////////////////////////////////////////////
# ------------------------------------- MAIN ----------------------------------------------------
if __name__ == "__main__":
    """Programa principal, donde se instancia la clase ServerHandler,
    indicando la IP, el puerto y el fichero de audio; donde se deja al servidor escuchando
    en un bucle infinito (del que solo se puede salir desde el terminal
    con Ctrl+C, que lanza una excepcion KeyboardInterrupt"""
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        AUDIO = sys.argv[3]
        if not os.path.isfile(AUDIO):
            sys.exit(AUDIO + ": File not found")
        else:
            serv = socketserver.UDPServer((IP, PORT), ServerHandler)
            print("Listening...")
            try:
                serv.serve_forever()
            except KeyboardInterrupt:
                print("Servidor Finalizado")
    except(IndexError, ValueError, PermissionError):
        sys.exit("Usage: python server.py IP port audio_file")


