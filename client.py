#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Módulo del cliente que abre un socket a un servidor."""""

import socket
import sys


# ///////////////////////////////////////////////////////////////////////////////
# A continuación encontramos una serie de metodos auxiliares que nos ayudaran a embellecer el código y
# mejorar su lectura, evitrando tener funciones largas que no cumplan los estandares PEP-8
# -------------------------------------------------------------------------------
def enviar_peticion():
    """Función que se usa para enviar una petición al servidor, independientemente del método que se reciba """
    global MESS
    MESS = METODO + ' sip:' + LOGIN + '@' + IP + ' SIP/2.0\r\n'
    sock.send(bytes(str(MESS), 'utf-8'))
    print('Enviando peticion... ')
    print(MESS)


# --------------------------------------------------------------------------------
def recibir():
    """Funcíon que se usa para recibir la respuesta del servidor"""
    data = sock.recv(1024)
    print('Recibiendo... ')
    print(data.decode('utf-8'))


# ----------------------------------------------------------------------------------
def invite_handler():
    """Funcion usada para enviar petición al servidor en caso del que el método usado sea INVITE """
    MENSAJE = 'ACK sip:' + LOGIN + '@' + IP + ' SIP/2.0\r\n'
    sock.send(bytes(str(MENSAJE), 'utf-8'))
    print('Enviando peticion...')
    print(MENSAJE)

# ////////////////////////////////////////////////////////////////////////////////////////
# --------- MAIN ------------------------------------------------------------------------
try:
    # En primer lugar, guardamos los parametros en variables constantes, ya que estos no sufriran modificaciones.
    METODO = sys.argv[1]
    RECEPTOR = sys.argv[2].split('@')
    LOGIN = RECEPTOR[0]
    IP = RECEPTOR[1].split(':')[0]
    PORT = int(RECEPTOR[1].split(':')[-1])
    # Creamos el socket
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        # y lo conectamos
        sock.connect((IP, PORT))
        enviar_peticion()
        recibir()
        if METODO == 'INVITE':
            # enviamos la peticion ACK de forma automática tras el INVITE, ya que es el unico metodo
            # que produce la respuesta que conlleva a la activacion del ACK
            invite_handler()
    print("Socket terminado con exito")

# Por último, comprobamos algunos errores
except (IndexError, ValueError):
    print("Usage: python3 client.py method receiver@IP:SIPport")
except ConnectionRefusedError:
    print("Servidor apagado")
